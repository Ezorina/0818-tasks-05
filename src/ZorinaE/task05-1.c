#include <stdio.h>
#include <stdlib.h>

struct BOOK
{
    char title[50];
    char author[30];
    int year;
};

int NumOfBooks()
{
    int count = 0;
    char buf[100];
    FILE *file;
    file = fopen("books.txt", "rt");
    while (fgets(buf, 100, file))
        count++;
    return count;
}

void GetBook(struct BOOK *book, char *str)
{
    int i = 0, j = 0;
    char temp[5];
    while (str[i] !=',')
    {
        book -> title[i] = str[i];
        i++;
    }
    i += 2;
    while (str[i] !=',')
    {
        book -> author[j] = str[i];
        i++;
        j++;
    }
    i += 2;
    j = 0;
    while (str[i] != '\0')
    {
        temp[j] = str[i];
        i++;
        j++;
    }
    book -> year = atoi(temp);
}

void SortByAuthor(struct BOOK *books, int count)
{
    struct BOOK temp;
    int i, j;
    for (i = 0; i < count - 1; i++)
        for (j = i + 1; j < count; j++)
            if (books[i].author[0] > books[j].author[0])
            {
                temp = books[i];
                books[i] = books[j];
                books[j] = temp;
            }
}

int main()
{
    struct BOOK *books;
    int i, maxy, miny, max = 0, min = 0;
    int count = NumOfBooks();
    char buf[100];
    FILE *fp;
    fp = fopen("books.txt", "rt");
    books = (struct BOOK*)calloc(count, sizeof(struct BOOK));
    for (i = 0; i < count; i++)
    {
        fgets(buf, 100, fp);
        GetBook(&books[i], buf);
    }
    fclose(fp);
    printf("List of books:\n");
    for (i = 0; i < count; i++)
        printf("%s, %s, %d\n", books[i].title, books[i].author, books[i].year);
    maxy = books[0].year;
    miny = books[0].year;
    for (i = 1; i < count; i++)
    {
        if (books[i].year > maxy)
        {
            maxy = books[i].year;
            max = i;
        }
        if (books[i].year < miny)
        {
            miny = books[i].year;
            min = i;
        }
    }
    printf("The oldest book:\n");
    printf("%s, %s, %d\n", books[min].title, books[min].author, books[min].year);
    printf("The newest book:\n");
    printf("%s, %s, %d\n", books[max].title, books[max].author, books[max].year);
    SortByAuthor(books, count);
    printf("Sorted by Authors:\n");
    for (i = 0; i < count; i++)
        printf("%s, %s, %d\n", books[i].title, books[i].author, books[i].year);
    return 0;
}