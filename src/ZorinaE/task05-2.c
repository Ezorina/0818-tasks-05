#include <stdio.h>
#include <string.h>
#define SIZE 256

int main()
{
    FILE *fp;
    int i, max = 0, maxnum = 0;
    int NumChar = 0, NumWord = 0, NumPunct = 0, NumNumer = 0, AverWord = 0;
    float NumLetter = 0;
    char MostChar;
    char buf[1024];
    int statistic[SIZE];
    fp = fopen("text.txt", "rt");
    for (i = 0; i < SIZE; i++)
        statistic[i] = 0;
    while(fscanf(fp, "%s", buf) != EOF)
    {
        NumWord++;
        for (i = 0; i < strlen(buf); i++)
            statistic[buf[i]]++;
        statistic[' ']++;
    }
    fclose(fp);
    for (i = 0; i < SIZE; i++)
    {
        NumChar+=statistic[i];
        if (statistic[i] > max)
        {
            max = statistic[i];
            maxnum = i;
        }
        if (i >= '0' && i <= '9')
            NumNumer += statistic[i];
        if (i >= 'a' && i <= 'z' ||  i >= 'A' && i <= 'Z')
            NumLetter += statistic[i];
    }
    NumPunct = statistic['.'] + statistic[','] + statistic['!'] + statistic['?'] + statistic['/'] + statistic['\''] + statistic[':'] + statistic[';'] + statistic['"'] + statistic['('] + statistic[')'];
    MostChar = maxnum;
    printf("Number of symbols: %d\n", NumChar);
    printf("Number of words: %d\n", NumWord);
    printf("Number of punctuation marks: %d\n", NumPunct);
    printf("Number of numerals: %d\n", NumNumer);
    printf("Average length of a word: %f\n", NumLetter/NumWord);
    printf("Most useful symbol: %c\n", MostChar);
    return 0;
}